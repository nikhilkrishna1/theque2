//
//  AppleMusicController.swift
//  theque
//
//  Created by William Jacob Ballard on 9/11/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import Foundation
import StoreKit

public class AppleMusicService {
    
    static let developerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlNON0cyQThaQjYifQ.eyJpc3MiOiJMNkg2NktVTFBGIiwiaWF0IjoxNTgxMjgwMTg1LCJleHAiOjE1ODEzMjMzODV9.aoU0ivc2J-xmCWg4nkB4Be9AGfcf4YenefLXqylLEvlWS3svfvepVsxg7zdN7oyEmK_4Eq_VJ-cwKy0PshIHVw"
    
    public static func authenticate() {
        SKCloudServiceController.requestAuthorization {
            (status: SKCloudServiceAuthorizationStatus) in
            switch status {
            case .authorized:
                print("authorized")
                return
            case .notDetermined:
                print("not determined")
                break
            case .denied:
                print("denied")
                return
            case .restricted:
                print("restricted")
                return
            }
        }
    }
    
    public static func checkIfDeviceCanPlayback() {
        let serviceController = SKCloudServiceController()
        serviceController.requestCapabilities {
            (capability : SKCloudServiceCapability, err : Error?) in
            switch capability {
            case .musicCatalogPlayback:
                print("Has Apple Music Subscription")
                
            case .addToCloudMusicLibrary:
                print("Really gotta figure out what this means")
                
            case .musicCatalogSubscriptionEligible:
                print("Apple Music Subscription Not Found")
            default:
                print("Shouldn't get this in relation to appleMusicCheckIfDeviceCanPlayback()")
            }
        }
    }
    
    public static func checkStorefrontCountryCode() {
        
        let serviceController = SKCloudServiceController()
        
        serviceController.requestStorefrontCountryCode { (countryCode, error) in
            print(countryCode!)
        }
    }
    
    public static func checkUserToken() {
        
        let serviceController = SKCloudServiceController()
        
        serviceController.requestUserToken(forDeveloperToken: developerToken) { (userToken, error) in
            print(userToken!)
        }
        
    }
    
    
    
    
}
