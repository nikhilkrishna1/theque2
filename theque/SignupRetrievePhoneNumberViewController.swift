//
//  SignupRetrievePhoneNumberViewController.swift
//  queIOS
//
//  Created by William Jacob Ballard on 8/21/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class SignupRetrievePhoneNumberViewController : UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    //Mark: Properties
    
    var number : Int = 0

    @IBOutlet weak var phoneNumber: UITextField!
    
//    @IBOutlet weak var submitNumberForVerification: UIButton!
    
    @IBOutlet weak var invalidNumberLabel: UILabel!
    
    
    @IBOutlet weak var countryCodePicker: UIPickerView!
    
    var countryCodePickerData: [String]!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        //Load country codes into an array for the UIPickerView
        
        
        
//        print(countryCodeArrayItem[0])
        countryCodePicker.delegate = self
        
        
        countryCodePicker.dataSource = self
        
    }
    
    func retrieveCountryCodePickerData() -> [String] {
        let fileURLProject = Bundle.main.path(forResource: "SupportedFirebaseCountriesAndCode", ofType: "txt")
        
        var readStringProject = ""
        
        do {
            
            readStringProject = try String(contentsOfFile: fileURLProject!, encoding: String.Encoding.utf8)
            
        } catch _ as NSError {
            print(fileURLProject ?? "NSError in retrieveCountCodePickerData()", "file didn't load")
        }
        
        

        print(readStringProject)
        
        return readStringProject.components(separatedBy: "\n")
        
        
    }
    
    
    
//    var countryCodeArrayItem : [String] = readStringProject.componentsSeparatedByString("\n")
    
    //Mark: Actions
    
//    print(countryCodeArrayItem[0])
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        countryCodePickerData = retrieveCountryCodePickerData()
        return countryCodePickerData.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        countryCodePickerData = retrieveCountryCodePickerData()
        return countryCodePickerData[row]
    }
    
    
    
    
    @IBAction func submitNumberButtonPressed(_ sender: Any) {
        
        if phoneNumber.text != nil {
            
            var number = phoneNumber.text
            
            print("-=-=-=-=-=-=--=", number!)
            
            if number!.isPhoneNumber() {
                
                print("DING DING DING VALIDATION")
                
//                number = "+18594576770"
//                number = "+18005557755"
                
//                let phoneNumber = Auth.auth().currentUser?.number!

                
                PhoneAuthProvider.provider().verifyPhoneNumber(number!, uiDelegate: nil) { (verificationID, error) in
                    if let error = error {
//                        self.showMessagePrompt(error.localizedDescription)
                        print("Some sort of error happened uh oh", error)
                        return
                    } else {
                        UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                        
                        print("Looks like we passed verification, what's up", verificationID!)
                        
                        self.onToVerify()
                    }
                

//                let phoneNumber = Auth.auth().currentUser?.phoneNumber
//                print(phoneNumber!)
//                print(ugrMgr.mobile!)
//                PhoneAuthProvider.provider().verifyPhoneNumber("+91" + ugrMgr.mobile!, uiDelegate: nil) { (verificationID, error) in
//                    if let error = error {
//                        print(error.localizedDescription)
//                        mainInstance.ShowAlertWithError(error.localizedDescription as NSString, msg: error.localizedDescription as NSString)
//                        return
//                    }
//                    self.verificationID = verificationID
//
//                }
                

                    
                    // Sign in using the verificationID and the code sent to the user
                    // ...
                }
            }
        } else {
            invalidNumberLabel.isHidden = false
        }
        
    }
    
    func onToVerify() -> Void {
        performSegue(withIdentifier: "VerificationSegue", sender: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

