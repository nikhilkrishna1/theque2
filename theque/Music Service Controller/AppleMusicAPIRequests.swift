//
//  AppleMusicAPIRequests.swift
//  theque
//
//  Created by William Jacob Ballard on 1/30/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import Foundation

public struct AppleMusicAPIRequests {
    
    static let scheme = "https"
    static let host = "api.music.apple.com"
    
    
    
    
    static func searchCatalog(term : String, countryCode : String) -> String {
        var components = URLComponents()
        components.scheme = self.scheme
        components.host = self.host
        components.path = "/v1/catalog/\(countryCode)/search"
        
        let noSpaceTerm = term.replacingOccurrences(of: " ", with: "+")
        
        var queryItems = [URLQueryItem]()
        let attributes = ["term":noSpaceTerm,
                          "limit":"25",
                          "types":"songs"]
        
        for (key,value) in attributes {
            queryItems.append(URLQueryItem(name: key, value: value))
            
        }
        components.queryItems = queryItems
        return components.url!.absoluteString
        
        
    }
    

    
    
    
    
}

 
