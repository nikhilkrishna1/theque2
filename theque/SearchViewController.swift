//
//  SearchViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 11/30/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class SearchViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
//    @IBOutlet weak var musicPlayerView: UIView!
//
//    @IBOutlet weak var musicPlayerImage: UIImageView!
//
//    @IBOutlet weak var musicPlayerSongTitle: UILabel!
//
//    @IBOutlet weak var musicPlayerSongArtist: UILabel!
//
//    @IBOutlet weak var musicPlayerPlayButton: UIButton!
//
//    @IBOutlet weak var musicPlayerForwardButton: UIButton!
    
    @IBOutlet weak var musicPlayerView: UIView!
    
    @IBOutlet weak var musicPlayerImage: UIImageView!
    
    @IBOutlet weak var musicPlayerSongArtist: UILabel!
    
    @IBOutlet weak var musicPlayerSongTitle: UILabel!
    
    @IBOutlet weak var musicPlayerPlayButton: UIButton!
    
    @IBOutlet weak var musicPlayerForwardButton: UIButton!
    
    
    typealias JSONstd = [String:AnyObject]
    
    var songs = [Song]()
    
    var spotifyURI = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        registerMusicPlayerGestures()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
        
        
        
        
        let xib = UINib.init(nibName: "SearchResultsCell", bundle: nil)
        
        tableView.register(xib, forCellReuseIdentifier: "SearchResultsCell")
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
            
        db.collection("User Play States").getDocuments() { (querySnapshot, err) in

            
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print(document.get("user") as? String  ?? "test")
                    if (document.get("user") as! String) == Auth.auth().currentUser!.uid {
                        
                        print(document.data())
                        self.musicPlayerSongTitle.text = document.get("songName") as? String
                        self.musicPlayerSongArtist.text = document.get("aristName") as? String
                        let img = document.get("image") as? String
                        let spotifyImageID = img!
                        print(spotifyImageID)
                        if spotifyImageID != "" {
                            let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex
                            
                            let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"

                            let imageUrl = URL(string: imageUrlString)!

                            let image = try? UIImage(withContentsOfUrl: imageUrl)
                            
                            self.musicPlayerImage.image = image!!
                        }
                        
                    }
                }
            }
        }
    }
    
    func callAlamo(url: String, isSpotify : Bool, completion : @escaping (() -> ())) {
        
        if isSpotify {
            let accessToken = (UIApplication.shared.delegate as! AppDelegate).sessionManager.session?.accessToken
            
            Alamofire.request(url, method: .get, headers: ["Authorization": "Bearer \(accessToken!)"]).validate().responseJSON(completionHandler: {
                response in
                switch response.result {
                case .success(let response):
                    self.parsing(json: response as! Data, isSpotify : isSpotify){
                        completion()}
                case .failure(let error):
                    print("FAILED TO REQUEST ALAMO")
                    
                }
            })
            
        } else {
            print(AppleMusicService.developerToken)
            //print(url)
            Alamofire.request(url, method: .get, headers: ["Authorization": "Bearer \(AppleMusicService.developerToken)"]).validate().responseJSON(completionHandler: {
                response in
                print(response.data!)
                switch response.result {
                case .success:
                    self.parsing(json: response.data!, isSpotify: false){
                        completion()}
                case .failure:
                    print(response.debugDescription)
            
                }
                //print(response.description)
                
                })
        }
        
    }
    
    func parsing(json: Data, isSpotify : Bool, completion : (() -> ())) {
        
        if isSpotify {
            do {
                let readable = try JSONSerialization.jsonObject(with: json, options: .mutableContainers) as! JSONstd
                if let tracks = readable["tracks"] as? JSONstd{
                    if let items = tracks["items"] as? [JSONstd] {
                        print(items)
                        for item in items {
                            let name = item["name"] as! String
                            print(name)
                            print("conversion happened")
                            let uri = item["uri"] as! String
                            self.spotifyURI.append(uri)
                            completion()
                        }
                    }
                }
            } catch {
                print("in here")
                print(error)
            }
        } else {
            do {
                print("seventy three")
                let readable = try JSONSerialization.jsonObject(with: json, options: []) as! JSONstd
                print("one")
                if let results = readable["results"] as? JSONstd {
                    print("two")
                    if let tracks = results["songs"] as? JSONstd{
                        //print("three")
                        if let items = tracks["data"] as? [JSONstd] {
                            //print(3)
                            //print(items)
                            songs = [Song]()
                            for item in items {
                                //print("for item in items")
                                if let attributes = item["attributes"] as? JSONstd {
                                    let name = attributes["name"] as! String
                                    let isrc = attributes["isrc"] as! String
                                    let artist = attributes["artistName"] as! String
                                    if let artwork = attributes["artwork"] as? JSONstd {
                                        var stringurl = artwork["url"] as! String
                                        if stringurl != "" {
                                            stringurl = stringurl.replacingOccurrences(of: "{h}", with: "100")
                                            stringurl = stringurl.replacingOccurrences(of: "{w}", with: "100")
                                            //print(stringurl)
                                            
    //                                        let mainImageData = NSData(contentsOf: url!)
    //                                        let mainImage = UIImage(data: mainImageData! as Data)
    //
                                            songs.append(Song(title: name, artist: artist, imageURL: stringurl, isrc: isrc))
                                            
                                            completion()
                                            
                                        } else {
                                            print("STRING URL EMPTY")
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch {
                print("in here for some strange reason")
                print(error)
            }
        }
        self.tableView.reloadData()
        
        
    }
    
    func registerMusicPlayerGestures() {
        
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeResponse(gesture:)))
        upSwipe.direction = .up
        
        self.musicPlayerView.addGestureRecognizer(upSwipe)
        
    }
    
    @objc func swipeResponse(gesture: UIGestureRecognizer) {
        
        if let swipeDirection = gesture as? UISwipeGestureRecognizer {
            switch  swipeDirection.direction {
            case .up:
                let db = Firestore.firestore()
                db.collection("User Play States").getDocuments() { (querySnapshot, err) in

                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            if (document.get("user") as! String) == Auth.auth().currentUser!.uid {
                                self.musicPlayerSongTitle.text = document.get("songName") as? String
                                self.musicPlayerSongArtist.text = document.get("aristName") as? String
                                let img = document.get("image") as? String
                                let spotifyImageID = img!
                                let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex
                                let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"

                                let imageUrl = URL(string: imageUrlString)!

                                let image = try? UIImage(withContentsOfUrl: imageUrl)
                                
                                self.musicPlayerImage.image = image!!
                            }
                        }
                    }
                }
                                
                self.performSegue(withIdentifier: "SearchToPlayer", sender: nil)
            default:
                print("Default for music player swipe")
            }
        }
        
    }
    
    @IBAction func pauseASong(_ sender: Any) {
        
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        let isPaused = (UIApplication.shared.delegate as! AppDelegate).isPaused

        if isPaused! {
            (sender as! UIButton).setImage(UIImage(systemName: "pause.fill"), for: UIControlState.normal)
            appRemote.playerAPI?.resume(nil)
        } else {
            (sender as! UIButton).setImage(UIImage(systemName: "play.fill"), for: UIControlState.normal)
            appRemote.playerAPI?.pause(nil)
        }

    }
    @IBAction func skipASong(_ sender: Any) {
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        appRemote.playerAPI?.skip(toNext: nil)
        //
    }
    //    @IBAction func skipASong() {
//
//        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
//        appRemote.playerAPI?.skip(toNext: nil)
//
//    }
//
//    @IBAction func pauseASong(_ sender: UIButton) {
//
//        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
//        let isPaused = (UIApplication.shared.delegate as! AppDelegate).isPaused
//
//        if isPaused! {
//            sender.setImage(UIImage(systemName: "pause.fill"), for: UIControlState.normal)
//            appRemote.playerAPI?.resume(nil)
//        } else {
//            sender.setImage(UIImage(systemName: "play.fill"), for: UIControlState.normal)
//            appRemote.playerAPI?.pause(nil)
//        }
//
//    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension SearchViewController : UITextFieldDelegate {
//
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        textField.resignFirstResponder()
//        return true
//    }
//
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        //update results here
//
//        //Need to implement iTunes to start on this
//
//        print(textField.text!)
//        callAlamo(url: AppleMusicAPIRequests.searchCatalog(term: textField.text!, countryCode: "us"))
//
//    }
//
//}

extension SearchViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("clicked")
        if searchBar.text != "" {
            print("stuff in search bar")
            callAlamo(url: AppleMusicAPIRequests.searchCatalog(term: searchBar.text!, countryCode: "us"), isSpotify: false){print("oops")}
            
        } else {
            print("nothing in search bar")
        }
        searchBar.resignFirstResponder()
    }

    
}

extension UIImageView {
    func addImage(from url: String) {
        //let request = URLRequest(url: url)
        Alamofire.request(url, method: .get).validate().responseData(completionHandler: {
            response in
            print("caught in here??")
            self.image = UIImage(data: response.data!)
        })
//        let mainImage = UIImage(data: mainImageData! as Data)
//        self.image = mainImage
    }
}

extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultsCell") as! SearchResultsCell
        
        
        cell.resultsTitle.text = songs[indexPath.row].title
        
        
        cell.resultsImage.addImage(from: songs[indexPath.row].imageURL!)
        print("I load after")
        //cell.resultsImage.addImage(from: songs[indexPath.row].imageURL!)
        
        cell.resultsInfo.text = songs[indexPath.row].artist
        
        //print(result.albumName!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //cell clicked, do something
        let song = self.songs[indexPath.row].isrc
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        getURI(from: song!) {
        
            appRemote.playerAPI?.play(self.spotifyURI, asRadio: false, callback: {(result, error) in
            if let res = result {
                print("result: \(res)")
            } else {
                print(error!)
            }
        })
        
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        }
    }
    
    func getURI(from isrc : String, completion : @escaping (() -> ())) {
        let url = "https://api.spotify.com/v1/search?type=track&q=isrc:\(isrc)"
        callAlamo(url: url, isSpotify: true) { completion()}
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.songs.count
    }
    
    
}

class SearchResultsCell : UITableViewCell {
    
    @IBOutlet weak var resultsImage: UIImageView!
    
    @IBOutlet weak var resultsTitle: UILabel!
    
    @IBOutlet weak var resultsInfo: UILabel!
    
    
}
