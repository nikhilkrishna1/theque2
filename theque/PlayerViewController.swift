//
//  PlayerViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 9/29/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Firebase

class PlayerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {

    
    var songName: String?
    var artistName: String?
    var image: UIImage?
    
    
    fileprivate var cellControllers = [CollectionCellController]()
    
    fileprivate let cellControllerFactory = PlayerCellFactory()
    
    fileprivate var items : [PlayerCellItems] = [
        PlayerCellItems.init(isPlayer: true, isQue: false)
    ]
    
    fileprivate let screenSize = CGSize(width: UIScreen.main.bounds.width - CGFloat(100), height: CGFloat(500))
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let collectionViewFlowLayout = UICollectionViewFlowLayout()
        
        collectionViewFlowLayout.estimatedItemSize = CGSize(width: collectionView.frame.width, height: 10)
        
        setUpCollectionView()
    }
    

    func setUpCollectionView() {
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    
        cellControllerFactory.registerCells(on: collectionView)
        
        //print(self.collectionView.frame.width)
        
        cellControllers = cellControllerFactory.cellControllers(with: items)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {

            let cell : MusicPlayer = cellControllers[indexPath.row].cellFromCollectionView(collectionView, forIndexPath: indexPath) as! MusicPlayer
            //modify play content here
            cell.playButton.addTarget(self, action: #selector(playButtonTapped(_:)), for: .touchUpInside)
            if let isPaused = (UIApplication.shared.delegate as! AppDelegate).isPaused {
                if !(isPaused) {
                    cell.playButton.setImage(#imageLiteral(resourceName: "pause"), for: UIControlState.normal)
                } else {
                    cell.playButton.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
                }
            } else {
                cell.playButton.setImage(#imageLiteral(resourceName: "play"), for: UIControlState.normal)
                
            }
            cell.skipButton.addTarget(self, action: #selector(skipButtonTapped(_:)), for: .touchUpInside)
            cell.backButton.addTarget(self, action: #selector(backButtonTapped(_:)), for: .touchUpInside)
            
            return cell
        }
        
        return cellControllers[indexPath.row].cellFromCollectionView(collectionView, forIndexPath: indexPath)
    }
    
    @objc func playButtonTapped(_ sender : UIButton) {
        print("play button tapped")
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        let isPaused = (UIApplication.shared.delegate as! AppDelegate).isPaused
        if isPaused! {
            appRemote.playerAPI?.resume(nil)
            self.collectionView.reloadData()
        } else {
            appRemote.playerAPI?.pause(nil)
            self.collectionView.reloadData()
        }
        
    }
    
    @objc func skipButtonTapped(_ sender : UIButton) {
        print("skip button tapped")
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        appRemote.playerAPI?.skip(toNext: nil)
        self.collectionView.reloadData()
        
    }
    
    @objc func backButtonTapped(_ sender : UIButton) {
        print("back button tapped")
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote

        appRemote.playerAPI?.skip(toPrevious: nil)
        self.collectionView.reloadData()
        
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        //print("In here")
//        cellControllers[indexPath.row].didSelectCell()
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print("called?")
        print(UIScreen.main.bounds.width)
        return CGSize(width: collectionView.frame.width, height: CGFloat(500))
    }
    
    
}

class PlayerCellFactory {
    
    func registerCells(on collectionView: UICollectionView) {
        
        //register navigation and social feed cells
        PlayerCellController.registerCell(on: collectionView)
    }
    
    func cellControllers(with items: [PlayerCellItems]) -> [CollectionCellController] {
        return items.map { item in
            
            if item.isPlayerItem {
                return PlayerCellController(item: item)
            } else {
                return PlayerCellController(item: item)
            }
        }
    }
}

struct PlayerCellItems {
    var isPlayerItem: Bool
    
    var isQueItem: Bool
    
    init(isPlayer : Bool, isQue : Bool) {
        self.isPlayerItem = isPlayer
        self.isQueItem = isQue
    }
    
}

class PlayerCellController : CollectionCellController {

    fileprivate let item : PlayerCellItems
    
    init(item: PlayerCellItems) {
        self.item = item
    }
    
    fileprivate static var cellIdentifier: String {
        return String(describing: type(of: MusicNavigationButtonView.self))
    }
    
    static func registerCell(on collectionView: UICollectionView) {

        let xib = UINib.init(nibName:"MusicPlayer", bundle: nil)
        
        collectionView.register(xib, forCellWithReuseIdentifier:"MusicPlayer")
        
        
        
    }

    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MusicPlayer = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicPlayer", for: indexPath) as! MusicPlayer
                
        let db = Firestore.firestore()
        db.collection("User Play States").getDocuments() { (querySnapshot, err) in

            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if (document.get("user") as! String) == Auth.auth().currentUser!.uid {
                        cell.playerTitle.text = document.get("songName") as? String
                        cell.playerArtist.text = document.get("aristName") as? String
                        let img = document.get("image") as? String
                        let spotifyImageID = img!
                        let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex
                        
                        let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"

                        let imageUrl = URL(string: imageUrlString)!

                        let image = try? UIImage(withContentsOfUrl: imageUrl)
                        
                        
                        cell.playerImage.image = image!!
                        
                        
                    }
                }
            }

        }
        //Do modification here
        print("here")
        
        print("here")
        
        //cell.contentView.isUserInteractionEnabled = false
        
        
        cell.backgroundColor = UIColor.purple
        print(item)
        if item.isPlayerItem {
            print("isPlayerItem")
    
        } else {
            print("isQueItem")
        }
        
        return cell
        
    }
    
    
    

    func didSelectCell() {
        
        if item.isPlayerItem {
            print("another")
            
        } else {
            print("Shouldn't get Cell like this")
        }
    }
    


}

class MusicPlayer : UICollectionViewCell {
    
    @IBOutlet weak var playerImage: UIImageView!
    
    @IBOutlet weak var playerTitle: UILabel!
    
    @IBOutlet weak var playerArtist: UILabel!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var skipButton: UIButton!
    
    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//
//        let screenSize = CGSize(width: UIScreen.main.bounds.width - CGFloat(50), height: CGFloat(500))
//        let autoLayoutAttributes = super.preferredLayoutAttributesFitting(layoutAttributes)
//
//        let targetSize = CGSize(width: screenSize.width, height: 0)
//
//        let autoLayoutSize = contentView.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.defaultLow)
//        let autoLayoutFrame = CGRect(origin: autoLayoutAttributes.frame.origin, size: autoLayoutSize)
//
//        autoLayoutAttributes.frame = autoLayoutFrame
//        return autoLayoutAttributes
//    }
//
    
}





