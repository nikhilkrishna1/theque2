//
//  MainTabBarController.swift
//  theque
//
//  Created by William Jacob Ballard on 1/19/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import MediaPlayer

class MainTabBarController: UITabBarController {

    var musicController : MPMusicPlayerController = MPMusicPlayerController.applicationMusicPlayer
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("main tab bar controller")
        // Do any additional setup after loading the view.
       
        

    }
    
    
    

    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension MainTabBarController : MPMediaPlayback {
//    func prepareToPlay() {
//        <#code#>
//    }
//
//    var isPreparedToPlay: Bool {
//        <#code#>
//    }
//
//    func play() {
//        <#code#>
//    }
//
//    func pause() {
//        <#code#>
//    }
//
//    func stop() {
//        <#code#>
//    }
//
//    var currentPlaybackTime: TimeInterval {
//        get {
//            <#code#>
//        }
//        set(currentPlaybackTime) {
//            <#code#>
//        }
//    }
//
//    var currentPlaybackRate: Float {
//        get {
//            <#code#>
//        }
//        set(currentPlaybackRate) {
//            <#code#>
//        }
//    }
//
//    func beginSeekingForward() {
//        <#code#>
//    }
//
//    func beginSeekingBackward() {
//        <#code#>
//    }
//
//    func endSeeking() {
//        <#code#>
//    }
//
//
//
//
//}
