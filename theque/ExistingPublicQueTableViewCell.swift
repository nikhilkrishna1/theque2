//
//  ExistingPublicQueTableViewCell.swift
//  theque
//
//  Created by Nikhil Krishna on 1/4/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit

class ExistingPublicQueTableViewCell: UITableViewCell {

    // outlets
    @IBOutlet weak var publicQueName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
