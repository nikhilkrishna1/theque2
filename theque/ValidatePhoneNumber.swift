//
//  ValidatePhoneNumber.swift
//  queIOS
//
//  Created by William Jacob Ballard on 8/22/19.
//  Copyright © 2019 The Que. All rights reserved.
//


// PLEASE NOTE THIS CODE IS TEMPORARY TO GET SIGNUP WORKING FULLY PROPERLY, JACOB
// WILL WRITE A PHONE NUMBER VALIDATOR BEFORE RELEASE
import Foundation

extension String {
    
    public func isPhoneNumber() -> Bool {
        if self.isDigits() == true {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isDigits() -> Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
}



