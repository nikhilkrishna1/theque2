//
//  AppDelegate.swift
//  theque
//
//  Created by William Jacob Ballard on 8/23/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SPTSessionManagerDelegate, SPTAppRemoteDelegate, SPTAppRemotePlayerStateDelegate {

    var window: UIWindow?

    var initialVC : UIViewController?

    var songInitial: String?
    var artistInitial: String?
    var imageInitial: UIImage?
    var currTrack: SPTAppRemoteTrack?
    var isPaused: Bool?
    var accessToken: String?
    let SpotifyClientID = "cc8cfb1f67e742cb9ec99319d27023f0"
    let SpotifyRedirectURL = URL(string: "spotify-ios-quick-start://spotify-login-callback")!

    lazy var configuration = SPTConfiguration(
      clientID: SpotifyClientID,
      redirectURL: SpotifyRedirectURL
    )
    
    lazy var sessionManager: SPTSessionManager = {
      if let tokenSwapURL = URL(string: "https://theque.herokuapp.com/api/token"),
         let tokenRefreshURL = URL(string: "https://theque.herokuapp.com/api/refresh_token") {
        print(tokenRefreshURL)
        self.configuration.tokenSwapURL = tokenSwapURL
        self.configuration.tokenRefreshURL = tokenRefreshURL
        self.configuration.playURI = nil
      }
      let manager = SPTSessionManager(configuration: self.configuration, delegate: self)
      return manager
    }()
    
    lazy var appRemote: SPTAppRemote = {
        let appRemote = SPTAppRemote(configuration: configuration, logLevel: .debug)
        appRemote.delegate = self
        return appRemote
    }()
    
    func sessionManager(manager: SPTSessionManager, didInitiate session: SPTSession) {
        print("success", session)
        self.accessToken = session.accessToken
        self.appRemote.connectionParameters.accessToken = session.accessToken
        self.appRemote.connect()
    }
    func sessionManager(manager: SPTSessionManager, didFailWith error: Error) {
      print("fail", error)
    }
    func sessionManager(manager: SPTSessionManager, didRenew session: SPTSession) {
      print("renewed", session)
    }




    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //Firebase Config
        FirebaseApp.configure()
        
        let requestedScopes: SPTScope = [.appRemoteControl]
        self.sessionManager.initiateSession(with: requestedScopes, options: .default)
        
        return true
    }
    
    func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        print("connected")
        self.appRemote.playerAPI?.delegate = self
        self.appRemote.playerAPI?.subscribe(toPlayerState: { (result, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
            }
        })
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        print("fail", error)
    }
    
    func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        print("disconenct")
    }
    
    func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        
        let db = Firestore.firestore()
        
        self.currTrack = playerState.track
        print("player state changes")
        print("isPaused", playerState.isPaused)
        self.isPaused = playerState.isPaused
        print("track.uri", playerState.track.uri)
        print("track.name", playerState.track.name)
        self.songInitial = playerState.track.name
        print("track.imageIdentifier", playerState.track.imageIdentifier)
        print("track.artist.name", playerState.track.artist.name)
        self.artistInitial = playerState.track.artist.name
        print("track.album.name", playerState.track.album.name)
        print("track.isSaved", playerState.track.isSaved)
        print("playbackSpeed", playerState.playbackSpeed)
        print("playbackOptions.isShuffling", playerState.playbackOptions.isShuffling)
        print("playbackOptions.repeatMode", playerState.playbackOptions.repeatMode.hashValue)
        print("playbackPosition", playerState.playbackPosition)
        
        NotificationCenter.default.post(name: NSNotification.Name("playStateChanged"),
         object: nil)
        
        
        let state = State(isPaused: playerState.isPaused, uri: playerState.track.uri, songName: playerState.track.name, image: playerState.track.imageIdentifier, artistName: playerState.track.artist.name)
        
        if let uid = Auth.auth().currentUser?.uid {
        
            db.collection("User Play States").document(uid).setData(
                
                state_to_dict(s: state)
                
            ) {err in
                if err != nil {
                    print("Somethin fucked up")
                } else {
                    print("Database addition worked")
                }
            }
        } else {
            
            db.collection("User Play States").addDocument(data: 
                
                state_to_dict(s: state)
                
            ) {err in
                if err != nil {
                    print("Somethin fucked up")
                } else {
                    print("Database addition worked")
                }
            }
            
        }
        

    }
    
    class State {
        
        var isPaused: Bool?
        var uri: String?
        var songName: String?
        var image: String?
        var artistName: String?
        
        init(isPaused: Bool, uri: String, songName: String, image: String, artistName: String) {
            
            self.isPaused = isPaused
            self.uri = uri
            self.songName = songName
            self.image = image
            self.artistName = artistName
            
        }
        
    }
    
    func state_to_dict(s: State) -> [String: Any] {
        
        let dict = ["user": Auth.auth().currentUser?.uid ?? 0, "isPaused": s.isPaused!, "uri": s.uri!, "songName": s.songName!, "image": s.image!, "aristName": s.artistName!] as [String : Any]
        return dict
        
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        if self.appRemote.isConnected {
            self.appRemote.disconnect()
        }
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.



    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        self.sessionManager.renewSession()
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.sessionManager.renewSession()
        if let _ = self.appRemote.connectionParameters.accessToken {
            self.appRemote.connect()
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let firebaseAuth = Auth.auth()
        firebaseAuth.setAPNSToken(deviceToken, type: AuthAPNSTokenType.sandbox)
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let firebaseAuth = Auth.auth()
        if (firebaseAuth.canHandleNotification(userInfo)){
            print(userInfo)
            return
        }
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
      self.sessionManager.application(app, open: url, options: options)
      Auth.auth().canHandle(url)
      return true
    }
    
    
    


}

