//
//  User.swift
//  theque
//
//  Created by William Jacob Ballard on 1/19/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import Foundation
import FirebaseDatabase.FIRDataSnapshot

class User {
    
    let uid : String
    
    let username : String
    
    init(uid: String, username: String) {
        
        self.uid = uid
        self.username = username
        
    }
    
    
}
