//
//  CreatePublicQueViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/3/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import MapKit
import FirebaseFirestore
import FirebaseAuth
import Firebase

class CreatePublicQueViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {

    // class variables and constants
    var db: Firestore!
    var locationManager = CLLocationManager()
    let annotation = MKPointAnnotation()
    var userLocation = CLLocationCoordinate2D()
    var placemark: CLPlacemark!
    var isPublic = true
    
    // outlets
    @IBOutlet weak var publicQueName: UITextField!
    @IBOutlet weak var finishedButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var publicButton: UIBarButtonItem!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationItem.hidesBackButton = true

        // request location access and update location
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        self.publicQueName.delegate = self
        
        self.finishedButton.isEnabled = false
        self.finishedButton.alpha = 0.5
        self.finishedButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        
        self.cancelButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        self.navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)


    }
    
    // MARK: Actions
    @IBAction func initializePublic() {
          
        // contact firestore and get userID and database
        db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        
        // create public que with proper attributes in Firestore
        db.collection("Public Ques").document(publicQueName.text!).setData([
            
            "name": publicQueName.text!,
            "latitude": self.userLocation.latitude,
            "longitude": self.userLocation.longitude,
            "city": self.placemark.locality!,
            "state": self.placemark.administrativeArea!,
            "songs": [[String:String]](),
            "members": [userID],
            "numUpvotes": [0],
            "likedSongUsersPublic": [[String:Bool]](),
            "chatMessages": [String](),
            "queMasters": [Auth.auth().currentUser!.uid]
            
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked")
            }
        }
        
        // get out of there
        self.dismiss(animated: true, completion: nil)
    }
    
    // switch to private
    @IBAction func toPrivateCreator() {

        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PrivateQueCreator")
        self.show(vc, sender: self)
            
    }
    
    @IBAction func returnHome() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    // MARK: Location Functions
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.userLocation.latitude = locValue.latitude
        self.userLocation.longitude = locValue.longitude
                
        let userLocation = locations.last
        let viewRegion = MKCoordinateRegionMakeWithDistance((userLocation?.coordinate)!, 600, 600)
        
        self.mapView.setRegion(viewRegion, animated: true)
        
        self.annotation.coordinate = CLLocationCoordinate2D(latitude: locValue.latitude, longitude: locValue.longitude)
        
        self.mapView.addAnnotation(annotation)
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: {
            
                placemarks, error -> Void in
                guard let placeMark = placemarks?.first else { return }
            
                self.placemark = placeMark
            
        })
        
    }
    
    // MARK: Text Field Functions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (self.publicQueName.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text.isEmpty {
            
            self.finishedButton.isEnabled = false
            self.finishedButton.alpha = 0.5
            
        } else {
            
            self.finishedButton.isEnabled = true
            self.finishedButton.alpha = 1.0
            
        }
        
        return true
    }
    
    
    
    

}
