
//
//  SpecificPublicQueTableViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/12/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth

class SpecificPublicQueViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIPopoverPresentationControllerDelegate {

    // class variables
    var publicQue = PublicQue()
    var filteredSongs = [[String:Any]]()
    var songNames = [String]()
    var artistNames = [String]()
    var db = Firestore.firestore()
    var flag = false
    var songToSend = [String:Any]()
    var indexSend: IndexPath!
    var refreshControl = UIRefreshControl()
    var menuShowing = false
    
    @IBOutlet weak var menu: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var addSongButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var playQueButton: UIButton!
    
    // MARK: Obj-C Functions
    @objc func refresh(sender:AnyObject) {
        let db = Firestore.firestore()
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if document.get("name") as! String == self.publicQue.name {
                        self.publicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]], chatMessages: document.get("chatMessages") as! [String], queMasters: document.get("queMasters") as! [String])
                        self.tableView.reloadData()
                    }
                }
            }
        }
        self.refreshControl.endRefreshing()
    }
    
    @objc func addSongToPublic() {
        
        if (menuShowing) {
            leadingConstraint.constant = -300
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseOut, animations: {
                           self.view.layoutIfNeeded()
            })
            self.tableView.isUserInteractionEnabled = true
        } else {
            leadingConstraint.constant = 0
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveEaseIn, animations: {
                self.view.layoutIfNeeded()
            })
            self.tableView.isUserInteractionEnabled = false

        }
        menuShowing = !menuShowing
    }
    
    @objc func handleLongPress(sender: UILongPressGestureRecognizer){
        if sender.state == UIGestureRecognizerState.began {
            let touchPoint = sender.location(in: self.tableView)
            if let indexPath = self.tableView.indexPathForRow(at: touchPoint) {
                UIImpactFeedbackGenerator().impactOccurred()
                self.indexSend = indexPath
                self.songToSend = self.filteredSongs[indexPath.row]
                self.performSegue(withIdentifier: "toSpecificSong", sender: nil)
            }
        }
    }
    
    @objc func toAddSong() {
        
        self.performSegue(withIdentifier: "toAddSongPublic", sender: nil)
        
    }
    
    @objc func closeMenu(sender: UISwipeGestureRecognizer) {
        switch sender.direction {
            case UISwipeGestureRecognizerDirection.left:
                self.leadingConstraint.constant = -300
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                })
                menuShowing = false
                self.tableView.isUserInteractionEnabled = true
            default:
                break
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        self.tableView.addGestureRecognizer(longPress)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(closeMenu))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.menu.addGestureRecognizer(swipeLeft)
        
        self.navigationItem.title = publicQue.name
        self.navigationItem.hidesBackButton = true
        
        let button: UIButton = UIButton(type: UIButtonType.custom)
        button.setImage(UIImage(systemName: "music.note.list"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(addSongToPublic), for: UIControlEvents.touchUpInside)
        button.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.isEnabled = false
        
        self.playQueButton.layer.cornerRadius = 15
        self.menu.layer.cornerRadius = 5
        
        self.tableView.rowHeight = 75
        let userID = Auth.auth().currentUser!.uid

        if self.publicQue.members.contains(userID) {
            let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.toAddSong))
            addBarButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
            self.navigationItem.leftBarButtonItem?.isEnabled = true
            self.navigationItem.rightBarButtonItems![0] = addBarButton
        }
        
        self.filteredSongs = self.publicQue.songs
        
        self.searchBar.delegate = self
        self.searchBar.placeholder = "Search songs in \(self.publicQue.name)"

        loadSongs()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.leadingConstraint.constant = -300
        self.tableView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        self.tableView.backgroundView = self.refreshControl
        self.tableView.insertSubview(refreshControl, at: 0)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.leadingConstraint.constant = 0
    }

    @objc func readData(_ notificaiton: Notification) {
        
        print("reading data...")
        let db = Firestore.firestore()
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
        if let err = err {
            print("Error getting documents: \(err)")
        } else {
            for document in querySnapshot!.documents {
                if document.get("name") as! String == self.publicQue.name {
                    self.publicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]], chatMessages: document.get("chatMessages") as! [String], queMasters: document.get("queMasters") as! [String])
                    self.tableView.reloadData()
                }
            }
            }
        }
    }



    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.filteredSongs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "specificPublicTableViewCell"

        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? SpecificPublicTableViewCell  else {
            fatalError("The dequeued cell is not an instance of specificPublicTableViewCell.")
        }

//        cell.songName.text = songNames[indexPath.row]
//        cell.artistName.text = artistNames[indexPath.row]
//        //CHANGE IMAGE LATER
        let db = Firestore.firestore()
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if document.get("name") as! String == self.publicQue.name {
                        let songs = document.get("songs") as! [[String:Any]]
                        self.publicQue.songs = songs
                        let song = self.filteredSongs[indexPath.row]
                        let imageUrlString = song["spotifyID"] as! String
                        cell.albumArt.downloaded(from: imageUrlString)
                        cell.artistName.text = song["artist"] as? String
                        cell.songName.text = self.filteredSongs[indexPath.row]["title"] as? String
                    }
                }
            }
        }

        //cell.numUpvotes.text = String(self.publicQue.upvotes[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if self.publicQue.songs.count != 1 {
                let songToDelete = self.publicQue.songs[indexPath.row]
                self.publicQue.songs = [self.publicQue.songs.remove(at: indexPath.row)]
                let db = Firestore.firestore()
                let queNameRef = db.collection("Public Ques").document(self.publicQue.name)
                queNameRef.updateData([
                    "songs": FieldValue.arrayRemove([songToDelete])
                ])
                self.tableView.reloadData()
            } else {

                let alertController = UIAlertController(title: "Cannot delete the only song in \(self.publicQue.name)", message: "Public Ques must have at least one song", preferredStyle: UIAlertControllerStyle.alert)
                
                alertController.addAction(UIAlertAction(title: "Got it.", style: UIAlertActionStyle.cancel, handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let position = touch.location(in: menu)
            if position.x > 302 {
                self.tableView.isUserInteractionEnabled = true
                self.leadingConstraint.constant = -300
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                    self.view.layoutIfNeeded()
                })
                menuShowing = false
            }
        }
    }
    
//
//
//    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
//
//        let db = Firestore.firestore()
//
//        let closeAction = UIContextualAction(style: .normal, title:  "Upvote", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
//
//            db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
//                       if let err = err {
//                           print("Error getting documents: \(err)")
//                       } else {
//                           for document in querySnapshot!.documents {
//                               if self.publicQue.name == document.get("name") as! String {
//                                self.publicQue.upvotes[indexPath.row] += 1
//                               }
//                           }
//                        db.collection("Public Ques").document(self.publicQue.name).updateData([
//
//                            "numUpvotes": self.publicQue.upvotes
//
//                        ]) {err in
//                            if err != nil {
//                                print("Somethin fucked up")
//                            } else {
//                                print("Database addition worked again")
//
//                            }
//                        }
//
//                    }
//            }
//            success(true)
//        })
//
//        closeAction.backgroundColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
//        return UISwipeActionsConfiguration(actions: [closeAction])
//
//    }
        
    // MARK: Actions

    @IBAction func connectToSpecificPublicQue() {
        
        let userID = Auth.auth().currentUser!.uid
        let db = Firestore.firestore()
        db.collection("Public Users").document(userID).setData([
            "publicMember": true
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked")
            }
        }
        
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if self.publicQue.name == document.get("name") as! String {
                        print(userID)
                        self.publicQue.members.append(userID)
                    }
                }
                let alertController = UIAlertController(title: "Success.", message:
                    "Added to " + self.publicQue.name, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Done", style: .default))

                self.present(alertController, animated: true, completion: nil)
                let addBarButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addSongToPublic))
                self.navigationItem.rightBarButtonItem = addBarButton
                }
            
            }
        
        db.collection("Public Ques").document(self.publicQue.name).updateData([
            "members": FieldValue.arrayUnion([userID])
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked again")
            }
        }
    }
    
    @IBAction func playAQue() {
        
        
        
        
        
    }
    
    @IBAction func addASong() {
        
        self.performSegue(withIdentifier: "toAddSongPublic", sender: nil)
        
    }
    
    @IBAction func gotoChat() {
        
        self.performSegue(withIdentifier: "toPublicQueChat", sender: nil)
    }
    
    // MARK: Search Functions
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.filteredSongs = searchText.isEmpty ? self.publicQue.songs : self.publicQue.songs.filter { (item: [String:Any]) -> Bool in
            let title = item["title"] as! String
            return title.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
            
        }
        
        self.tableView.reloadData()
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
    }

    // MARK: Helper Functions
    func loadSongs() {
        
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                
                    let newPublicQue = PublicQue(name: document.get("name") as! String, latitude: document.get("latitude") as! Float64, longitude: document.get("longitude") as! Float64, city: document.get("city") as! String, state: document.get("state") as! String, songs: document.get("songs") as! [[String:Any]], members: document.get("members") as! [String], upvotes: document.get("numUpvotes") as! [Int], likedSongUsersPublic: document.get("likedSongUsersPublic") as! [[String:Bool]], chatMessages: document.get("chatMessages") as! [String], queMasters: document.get("queMasters") as! [String])
                    
                    if newPublicQue.name == self.publicQue.name {
                        
                        self.songNames = self.getSongAttributes(publicQue: self.publicQue, attr: "title") as! [String]
                        self.artistNames = self.getSongAttributes(publicQue: self.publicQue, attr: "artist") as! [String]
                    }
                    
                    
                }
            }
            self.tableView.reloadData()
        }
        
        
        
    }
    
    func getSongAttributes(publicQue: PublicQue, attr: String) -> [String?] {

        var res = [String?]()

        if attr == "title" {
            for song in publicQue.songs {
                res.append(song["title"] as? String)
            }
        } else if attr == "artist" {
            for song in publicQue.songs {
                res.append(song["artist"] as? String)
            }
        }

        return res
        
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Optional("toPublicQueChat") {
            let publicQueChat: PublicQueChatViewController = segue.destination as! PublicQueChatViewController
            publicQueChat.publicQue = self.publicQue
            
        }
        
        if segue.identifier == Optional("toAddSongPublic") {
            let addSongPublicVC: AddSongToPublicTableViewController = segue.destination as! AddSongToPublicTableViewController
            addSongPublicVC.publicQue = self.publicQue
        }
        
        if segue.identifier == Optional("toSpecificSong") {
            let specificSongVC: SpecificSongViewController = segue.destination as! SpecificSongViewController
            let pop = segue.destination.popoverPresentationController
            pop!.sourceRect = tableView.cellForRow(at: indexSend)!.bounds
            pop!.sourceView = tableView.cellForRow(at: indexSend)!
            pop!.delegate = self
            specificSongVC.song = self.songToSend
            specificSongVC.publicQue = self.publicQue
        }
    }
    

    
}
