//
//  SignupViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 1/19/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class PhoneNumberInputViewController : UIViewController {
    
    var db : Firestore!
    
    @IBOutlet weak var phoneNumberField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Should be in here")
        
        self.view.backgroundColor = .red
        
    
        
        
        

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        
        
        
        
        if Auth.auth().currentUser != nil {
            print("Already signed in yep")
            print(DispatchQueue.isMember(.main))
            
            self.performSegue(withIdentifier: "alreadyLoggedIn", sender: nil)
            
        } else {
            
            print("Not signed in")
        }
        
    }
    
    
    @IBAction func phoneNumberFieldDidEdit(_ sender: Any) {
        print("Did edit")
        
    }
    
    @IBAction func loginButtonTapped(_ sender: Any) {
        
        
    }
    
    @IBAction func signupButtonTapped(_ sender: Any) {
        
        print("signup button tap")
        
        
        self.view.backgroundColor = .black
        authNumber()
        
    }
    
    
    
    func authNumber() {
        if phoneNumberField.text != nil {
                    
            let number = phoneNumberField.text
            
            print("-=-=-=-=-=-=--=", number!)
            
            //if number!.isPhoneNumber() {
                
    //                number = "+18594576770"
    //                number = "+18005557755"
                
    //                let phoneNumber = Auth.auth().currentUser?.number!

                PhoneAuthProvider.provider().verifyPhoneNumber(number!, uiDelegate: nil) { (verificationID, error) in
                    if let error = error {

                        print("Some sort of error happened uh oh", error)
                        return
                    } else {
                        UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                        
                        print("Looks like we passed verification, what's up", verificationID!)
                        
                        
                        self.db = Firestore.firestore()
                        
                //let userID = Auth.auth().currentUser!.uid
                       
                        self.db.collection("Public Users").whereField("number", isEqualTo: number!).getDocuments() { (querySnapshot, err) in
                            
                            if err != nil {
                                
                                print("User does not exist")
                                self.onToVerify(newUser: true, number: number!)
                                
                            } else {
                                
                                print("Got a match")
                                self.onToVerify(newUser: false, number: number!)
                            }
                            
                            
                            
                        }
                        
                        
                        
                        //self.onToVerify()
                    }
                }
           }
       //} else {
            
            
           //invalidNumberLabel.isHidden = false
       //}
        
        
    }
    
    func retrieveCountryCodePickerData() -> [String] {
        let fileURLProject = Bundle.main.path(forResource: "SupportedFirebaseCountriesAndCode", ofType: "txt")
        
        var readStringProject = ""
        
        do {
            
            readStringProject = try String(contentsOfFile: fileURLProject!, encoding: String.Encoding.utf8)
            
        } catch let error as NSError {
            print("retrieveCountryCodePickerData() file didn't load", error)
        }
        
        

        print(readStringProject)
        
        return readStringProject.components(separatedBy: "\n")
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
                        
    func onToVerify(newUser : Bool, number : String) -> Void {
        
        if !newUser {
            performSegue(withIdentifier: "VerificationSegue", sender: nil)
        } else {
            performSegue(withIdentifier: "VerificationSegue", sender: number)
        }
        
    }

}

class SignupNumberVerificationViewController : UIViewController {
    
    var db : Firestore!
    
    var isNewUser : Bool!
    
    var phoneNumber : String!
    
    @IBOutlet weak var verificationCodeField: UITextField!
    
    @IBOutlet weak var verifyButton: UIButton!
    
    let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
    
    var code = "invalid"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "VerificationSegue" {
            print("VerificationSegue identified and prepared")
            if sender != nil {
                isNewUser = true
                phoneNumber = sender as? String
                print("Verification VC isNewUser", isNewUser, phoneNumber)
            } else {
                isNewUser = false
                print("Verification VC isNewUser", isNewUser)
            }
            
        }
    }
    
    
    @IBAction func verificationCodeFieldDidEdit(_ sender: Any) {
        
        authVerificationCode()
        
    }
    
    
    @IBAction func verifyButtonTapped(_ sender: Any) {
        
        authVerificationCode()
        
    }
    
    func authVerificationCode() {
        
        if verificationCodeField.text != nil {
            code = verificationCodeField.text!
        }
        print("())()()*()()()()()()()!(@)(#)@!)(#)@!()#(@)!(#)!()!(#)!@()#(@!)(#)!@()9a", code)
        
        if code.count == 6 {
    
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: code)
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if let _ = error {
                    // ...
                    print("ERROR SIGNING IN USER TO FIREBASE!")
                    return
                }
                // User is signed in
                // ...
                print("Signin Successful!")
                
                self.pushPhoneNumberToDB()
                
                self.performSegue(withIdentifier: "LoggedIn", sender: nil)
            }
            
                    
        } else {
                    
//            invalidCodeLabel.isHidden = false
            print("yuh oh")
        
        }
    }
    
    func pushPhoneNumberToDB() {
        
        db = Firestore.firestore()
        
        let userID = Auth.auth().currentUser!.uid
        
        db.collection("Public Users").document(userID).setData([
            "number": self.phoneNumber
        ]) {err in
            if err != nil {
                print("fucked it bad")
            } else {
                print("Added user number to database")
            }
        }
        
    }
    
    
}
