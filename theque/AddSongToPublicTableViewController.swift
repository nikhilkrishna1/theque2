//
//  AddSongToPublicTableViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/13/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

class AddSongToPublicTableViewController: UITableViewController, UISearchBarDelegate {

    // class variables
    var songs = [Song]()
    var selectedSong: Song!
    var filteredData: [String]!
    var searchURL = String()
    var nameToSend = String()
    var imageToSend: UIImage!
    var publicQue: PublicQue!
    
    typealias JSONstd = [String:AnyObject]
    // outlets
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = 75
        searchBar.delegate = self

        searchBar.becomeFirstResponder()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return songs.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "songIdentifier", for: indexPath) as! AddSongToPublicTableViewCell

        //cell.songName.text = results[indexPath.row]
        cell.albumArt.image = songs[indexPath.row].image
        cell.songName.text = songs[indexPath.row].title
        cell.artistName.text = songs[indexPath.row].artist
        return cell
    }


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        self.nameToSend = songs[indexPath.row].title!
        self.imageToSend = songs[indexPath.row].image
        

        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let db = Firestore.firestore()
        let queNameRef = db.collection("Public Ques").document(self.publicQue.name)
        queNameRef.updateData([
            "songs": FieldValue.arrayUnion([song_to_dict(song: songs[indexPath.row])])
        ])
        self.selectedSong = songs[indexPath.row]
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: Search Bar Functions
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(reload), object: nil)
            self.perform(#selector(reload), with: nil, afterDelay: 0.7)
        self.tableView.reloadData()
    }
    
    @objc func reload() {
        guard let searchText = searchBar.text else { return }
        let keywords = searchText
        let final = keywords.replacingOccurrences(of: " ", with: "%20")
        searchURL = "https://api.spotify.com/v1/search?q=\(final)&type=track"
        self.callAlamo(url: searchURL)
        self.tableView.reloadData()
    }

    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.endEditing(true)
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let keywords = searchBar.text
        let final = keywords?.replacingOccurrences(of: " ", with: "%20")
        searchURL = "https://api.spotify.com/v1/search?q=\(final!)&type=track"
        self.callAlamo(url: searchURL)
        self.searchBar.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        songs = [Song]()
        self.tableView.reloadData()
    }
    
    // MARK: AlamoFire
    func callAlamo(url: String) {
        
        let accessToken = (UIApplication.shared.delegate as! AppDelegate).sessionManager.session?.accessToken
        
        Alamofire.request(url, method: .get, headers: ["Authorization": "Bearer \(accessToken!)"]).validate().responseJSON(completionHandler: {
            response in
            switch response.result {
            case .success(let response):
                self.parsing(json: response as! Data)
            case .failure(let error):
                print("FAILED TO REQUEST ALAMO")
            }
        })
    }
    
    func parsing(json: Data) {
        do {
            let readable = try JSONSerialization.jsonObject(with: json, options: .mutableContainers) as! JSONstd
            if let tracks = readable["tracks"] as? JSONstd{
                if let items = tracks["items"] as? [JSONstd] {
                    for item in items {
                        let name = item["name"] as! String
                        let uri = item["uri"] as! String
                        if let artists = item["artists"] as? [JSONstd] {
                            if let album = item["album"] as? JSONstd {
                                if let images = album["images"] as? [JSONstd] {
                                    let imageData = images[0]
                                    let imageURL = URL(string: imageData["url"] as! String)
                                    let mainImageData = NSData(contentsOf: imageURL!)
                                    let mainImage = UIImage(data: mainImageData! as Data)
                                    songs.append(Song(title: name, artist: artists[0]["name"] as! String, image: mainImage!, spotifyID: imageData["url"] as! String, spotifyURI: uri))
                                    self.tableView.reloadData()
                                    
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            print("in here")
            print(error)
        }
        
        
    }


    /*
     Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
         Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
     Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
             Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
             Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
     Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
     Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
         Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("in here")
        if segue.identifier == Optional("addThisSong") {
            let vc: SpecificPublicQueViewController = segue.destination as! SpecificPublicQueViewController
            let db = Firestore.firestore()
            db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        if document.get("name") as! String == self.publicQue.name {
                            vc.publicQue.songs.append(self.song_to_dict(song: self.selectedSong))
                            vc.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func song_to_dict(song: Song) -> [String:Any] {
        
        let dict = ["title": song.title!, "artist": song.artist!, "spotifyID": song.spotifyID!, "spotifyURI": song.spotifyURI!] as [String : Any]
        return dict
        
    }
}
