//
//  CustomRadiusViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/7/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import MapKit

class CustomRadiusViewController: UIViewController, CLLocationManagerDelegate {

    // class variables and constants
    let annotation = MKPointAnnotation()
    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D()
    var placemark: CLPlacemark!
    var regionLocation = BoundaryRegion(northWestCorner: CLLocationCoordinate2D(), southEastCorner: CLLocationCoordinate2D())
    
    // outlets
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var sliderRadius: UISlider!
    @IBOutlet weak var radiusControl: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sliderRadius.thumbTintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        sliderRadius.minimumTrackTintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        
        // ask user for location and update
        self.locationManager.requestWhenInUseAuthorization()
    
        if CLLocationManager.locationServicesEnabled() {
           
           locationManager.delegate = self
           locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
           locationManager.startUpdatingLocation()
        }
                
    }
    
    // MARK: Actions
    
    // for slider
    @IBAction func sliderValueChange(sender: UISlider) {
        // Change to custom 
        self.radiusControl.selectedSegmentIndex = 3
        
        let currValue = sender.value / 3.28084
        var coordinate = CLLocationCoordinate2D()

        coordinate.latitude = self.userLocation.latitude
        coordinate.longitude = self.userLocation.longitude

        let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, Double(currValue), Double(currValue))
        
        locationManager.stopUpdatingLocation()

        self.mapView.setRegion(viewRegion, animated: true)
        
        let region = self.mapView.region
        let center = region.center
        
        // Set region location coordinates on map for radius
        self.regionLocation.northWestCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0)
        self.regionLocation.northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0)
        self.regionLocation.southEastCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0)
        self.regionLocation.southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0)
        
    }
    
    // for segmented control
    @IBAction func customSegmentRadiusChange(_ sender: UISegmentedControl) {
        var coordinate = CLLocationCoordinate2D()
        
        coordinate.latitude = self.userLocation.latitude
        coordinate.longitude = self.userLocation.longitude
        
        if self.radiusControl.selectedSegmentIndex == 0 {

            let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 50000.0/3.28084, 50000.0/3.28084)

            self.mapView.setRegion(viewRegion, animated: true)
            let region = self.mapView.region
            let center = region.center

            // Set region location coordinates on map for radius
            self.regionLocation.northWestCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0)
            self.regionLocation.northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0)
            self.regionLocation.southEastCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0)
            self.regionLocation.southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0)

            locationManager.stopUpdatingLocation()
        }
        if self.radiusControl.selectedSegmentIndex == 1 {
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 5280.0/3.28084, 5280/3.28084)
            
            self.mapView.setRegion(viewRegion, animated: true)
            let region = self.mapView.region
            let center = region.center
                        
            // Set region location coordinates on map for radius
            self.regionLocation.northWestCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0)
            self.regionLocation.northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0)
            self.regionLocation.southEastCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0)
            self.regionLocation.southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0)
            
            locationManager.stopUpdatingLocation()
        }
        
        if self.radiusControl.selectedSegmentIndex == 2 {
            
            let viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 26400.0/3.28084, 26400.0/3.28084)
            
            self.mapView.setRegion(viewRegion, animated: true)
            let region = self.mapView.region
            let center = region.center
                        
            // Set region location coordinates on map for radius
            self.regionLocation.northWestCorner.latitude  = center.latitude  - (region.span.latitudeDelta  / 2.0)
            self.regionLocation.northWestCorner.longitude = center.longitude - (region.span.longitudeDelta / 2.0)
            self.regionLocation.southEastCorner.latitude  = center.latitude  + (region.span.latitudeDelta  / 2.0)
            self.regionLocation.southEastCorner.longitude = center.longitude + (region.span.longitudeDelta / 2.0)
            
            locationManager.stopUpdatingLocation()
        }
        
        
    }
    
    // for finished button
    @IBAction func finishRadius() {
        self.performSegue(withIdentifier: "finishedRadius", sender: nil)
    }
    
    
    // MARK: Location Functions
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        self.userLocation.latitude = locValue.latitude
        self.userLocation.longitude = locValue.longitude
                                
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
        
        self.annotation.coordinate = CLLocationCoordinate2D(latitude: self.userLocation.latitude, longitude: self.userLocation.longitude)
        
        self.mapView.addAnnotation(annotation)
        
        let viewRegion = MKCoordinateRegionMakeWithDistance(self.userLocation, 5000.0/3.28084, 5000.0/3.28084)
        
        self.mapView.setRegion(viewRegion, animated: true)
        locationManager.stopUpdatingLocation()

        geoCoder.reverseGeocodeLocation(location, completionHandler: {
                placemarks, error -> Void in
                guard let placeMark = placemarks?.first else { return }
                self.placemark = placeMark
        })
        
    }
    
    // MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == Optional("finishedRadius") {
            let existingPublicVC: ExistingPublicQueTableViewController = segue.destination as! ExistingPublicQueTableViewController
            existingPublicVC.regionLocation.northWestCorner = self.regionLocation.northWestCorner
            existingPublicVC.regionLocation.southEastCorner = self.regionLocation.southEastCorner
            existingPublicVC.flag = false
            existingPublicVC.tableView.reloadData()
            
        }
    }
    
        
    
    
    
}

