//
//  PublicQue.swift
//  theque
//
//  Created by Nikhil Krishna on 1/7/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit

class PublicQue {
    
    var name: String
    var latitude: Float64
    var longitude: Float64
    var city: String
    var state: String
    var songs: [[String:Any]]
    var members: [String]
    var upvotes: [Int]
    var likedSongUsersPublic: [[String: Bool]]
    var chatMessages: [String]
    var queMasters: [String]
    
    init() {
        
        self.name = ""
        self.latitude = 0.0
        self.longitude = 0.0
        self.city = ""
        self.state = ""
        self.songs = [[String:Any]]()
        self.members = [String]()
        self.upvotes = [0]
        self.likedSongUsersPublic = [[String: Bool]]()
        self.chatMessages = []
        self.queMasters = []
    }
    
    init(name: String, latitude: Float64, longitude: Float64, city: String, state: String, songs: [[String:Any]], members: [String], upvotes: [Int], likedSongUsersPublic: [[String: Bool]], chatMessages: [String], queMasters: [String]) {
        
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.city = city
        self.state = state
        self.songs = songs
        self.members = members
        self.upvotes = upvotes
        self.likedSongUsersPublic = likedSongUsersPublic
        self.chatMessages = chatMessages
        self.queMasters = queMasters
    }
    
}
