//
//  CreatePrivateQueTableViewCell.swift
//  theque
//
//  Created by Nikhil Krishna on 1/14/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import Contacts

class CreatePrivateQueTableViewCell: UITableViewCell {
    
    // class variables
    var phoneNumbers = [CNLabeledValue<CNPhoneNumber>]()
    
    // outlets
    @IBOutlet weak var contactNameLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
