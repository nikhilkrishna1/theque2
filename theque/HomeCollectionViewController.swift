//
//  HomeCollectionCollectionViewController.swift
//  theque
//
//  Created by William Jacob Ballard on 12/9/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import UIKit
import Firebase

private let reuseIdentifier = "Cell"

protocol CollectionCellController {
    
    static func registerCell(on collectionView: UICollectionView)
    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell
    func didSelectCell()
    
}

extension UIImage {

    convenience init?(withContentsOfUrl url: URL) throws {
        let imageData = try Data(contentsOf: url)
    
        self.init(data: imageData)
    }

}




class HomeCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    

    fileprivate var cellControllers = [CollectionCellController]()
    
    //fileprivate var items = [HomeCellItems]
    
    fileprivate let cellControllerFactory = HomeCellFactory()
    
    fileprivate var items : [CellItems] = [
        CellItems.init(isSocial: false, isNav: true, isJoin: true, isCreate: false, isSong: false, isLiked: false, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: true, isSong: false, isLiked: false, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: false, isSong: true, isLiked: false, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: false, isSong: false, isLiked: true, isPlaylist: false),
        CellItems.init(isSocial: false, isNav: true, isJoin: false, isCreate: false, isSong: false, isLiked: false, isPlaylist: true)
    ]
    
    fileprivate let screenSize = CGSize(width: UIScreen.main.bounds.width, height: CGFloat(40))

    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var musicPlayerView: UIView!
    
    @IBOutlet weak var musicPlayerImage: UIImageView!
    
    @IBOutlet weak var musicPlayerSongTitle: UILabel!
    
    @IBOutlet weak var musicPlayerSongArtist: UILabel!
    
    @IBOutlet weak var musicPlayerPlayButton: UIButton!
    
    @IBOutlet weak var musicPlayerForwardButton: UIButton!


    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewWillAppear(false)
        NotificationCenter.default.addObserver(self, selector:#selector(getLabels), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        self.collectionView.reloadData()
        setUpCollectionView()
        registerMusicPlayerGestures()
        
        
        self.navigationItem.hidesBackButton = true

    }
    
    @objc func getLabels() {
        
        let db = Firestore.firestore()
        db.collection("User Play States").getDocuments() { (querySnapshot, err) in

            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if (document.get("user") as! String) == Auth.auth().currentUser!.uid {
                        self.musicPlayerSongTitle.text = document.get("songName") as? String
                        self.musicPlayerSongArtist.text = document.get("aristName") as? String
                        let img = document.get("image") as? String
                        let spotifyImageID = img!
                        let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex
                        let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"

                        let imageUrl = URL(string: imageUrlString)!

                        let image = try? UIImage(withContentsOfUrl: imageUrl)
                        
                        self.musicPlayerImage.image = image!!
                        self.collectionView.reloadData()
                    }
                }
            }
        }
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()

        db.collection("User Play States").getDocuments() { (querySnapshot, err) in

            
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    print(document.get("user") as? String  ?? "test")
                    if (document.get("user") as! String) == Auth.auth().currentUser!.uid {
                        
                        print(document.data())
                        self.musicPlayerSongTitle.text = document.get("songName") as? String
                        self.musicPlayerSongArtist.text = document.get("aristName") as? String
                        let img = document.get("image") as? String
                        let spotifyImageID = img!
<<<<<<< HEAD
                        print(spotifyImageID)
                        if spotifyImageID != "" {
                            let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex
                            
                            let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"
=======
                        let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex

                        let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"
>>>>>>> 6e4505d1aaf4de053f27801dd74e76d59a5cba23

                            let imageUrl = URL(string: imageUrlString)!

<<<<<<< HEAD
                            let image = try? UIImage(withContentsOfUrl: imageUrl)
                            
                            self.musicPlayerImage.image = image!!
                        }
                        
=======
                        let image = try? UIImage(withContentsOfUrl: imageUrl)

                        self.musicPlayerImage.image = image!!
>>>>>>> 6e4505d1aaf4de053f27801dd74e76d59a5cba23
                    }
                }
            }
        }
    }
    

    func registerMusicPlayerGestures() {
        
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(swipeResponse(gesture:)))
        upSwipe.direction = .up
        
        self.musicPlayerView.addGestureRecognizer(upSwipe)
        
    }
    
    @objc func swipeResponse(gesture: UIGestureRecognizer) {
        
        if let swipeDirection = gesture as? UISwipeGestureRecognizer {
            switch  swipeDirection.direction {
            case .up:
                let db = Firestore.firestore()
                db.collection("User Play States").getDocuments() { (querySnapshot, err) in

                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        for document in querySnapshot!.documents {
                            if (document.get("user") as! String) == Auth.auth().currentUser!.uid {
                                self.musicPlayerSongTitle.text = document.get("songName") as? String
                                self.musicPlayerSongArtist.text = document.get("aristName") as? String
                                let img = document.get("image") as? String
                                let spotifyImageID = img!
                                let range = spotifyImageID.index(after: spotifyImageID.index((spotifyImageID.startIndex), offsetBy: 13))..<spotifyImageID.endIndex
                                let imageUrlString = "https://i.scdn.co/image/\(spotifyImageID[range])"

                                let imageUrl = URL(string: imageUrlString)!

                                let image = try? UIImage(withContentsOfUrl: imageUrl)
                                
                                self.musicPlayerImage.image = image!!
                            }
                        }
                    }
                }
                                
                self.performSegue(withIdentifier: "MusicPlayerSegue", sender: nil)
            default:
                print("Default for music player swipe")
            }
        }
        
    }
    
    
    func setUpCollectionView() {
        
        self.collectionView.dataSource = self
        
        self.collectionView.delegate = self
        
        self.collectionView.isUserInteractionEnabled = true
        
        cellControllerFactory.registerCells(on: collectionView)
        
        
        cellControllers = cellControllerFactory.cellControllers(with: items)
        
        NotificationCenter.default.addObserver(self, selector: #selector(createQueSegue(_:)), name: Notification.Name("createQueButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(joinQueSegue(_:)), name: Notification.Name("joinQueButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(songPushToNav(_:)), name: Notification.Name("songButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(likedPushToNav(_:)), name: Notification.Name("likedButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(playlistPushToNav(_:)), name: Notification.Name("playlistButtonTapped"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(_:)), name: Notification.Name("playStateChanged"), object: nil)
        
        
    }
    
    @objc func refresh(_ notification: Notification) {
        
        self.viewWillAppear(false)
        
    }
    @objc func createQueSegue(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "MainToCreate", sender: nil)
        
    }
    
    @objc func joinQueSegue(_ notification: Notification) {
        
        self.performSegue(withIdentifier: "MainToJoin", sender: nil)
    }
    
    @objc func songPushToNav(_ notification: Notification) {
        
        print("Does somethign to nav")
        //push to na
        
    }
    
    @objc func likedPushToNav(_ notification: Notification) {
        
        //push to na
        
    }
    
    @objc func playlistPushToNav(_ notification: Notification) {
        
        //push to na
        
    }
    
    @IBAction func skipASong() {
        
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        self.musicPlayerPlayButton.setImage(UIImage(systemName: "pause.fill"), for: UIControlState.normal)
        appRemote.playerAPI?.skip(toNext: nil)
        
    }
    
    @IBAction func pauseASong(_ sender: UIButton) {
        
        let appRemote = (UIApplication.shared.delegate as! AppDelegate).appRemote
        let isPaused = (UIApplication.shared.delegate as! AppDelegate).isPaused

        if isPaused! {
            sender.setImage(UIImage(systemName: "pause.fill"), for: UIControlState.normal)
            appRemote.playerAPI?.resume(nil)
        } else {
            sender.setImage(UIImage(systemName: "play.fill"), for: UIControlState.normal)
            appRemote.playerAPI?.pause(nil)
        }
        
    }


    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 5
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        return cellControllers[indexPath.row].cellFromCollectionView(collectionView, forIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //print("In here")
        cellControllers[indexPath.row].didSelectCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return screenSize
    }
    

}

class HomeCellFactory {
    
    func registerCells(on collectionView: UICollectionView) {
        
        //register navigation and social feed cells
        NavigationButtonCellController.registerCell(on: collectionView)
    }
    
    func cellControllers(with items: [CellItems]) -> [CollectionCellController] {
        return items.map { item in
            
            if item.isNavigationButtonCellItem {
                return NavigationButtonCellController(item: item)
            } else {
                return NavigationButtonCellController(item: item)
            }
        }
        
    }
    
    
    
    
}


struct CellItems {
    var isSocialFeedItem: Bool
    
    var isNavigationButtonCellItem: Bool
    
    var isJoinQueItem : Bool
    
    var isCreateQueItem : Bool
    
    var isSongItem : Bool
    
    var isLikedItem : Bool
    
    var isPlaylistsItem : Bool
    
    init(isSocial : Bool, isNav : Bool, isJoin : Bool, isCreate : Bool, isSong : Bool, isLiked : Bool, isPlaylist : Bool) {
        self.isSocialFeedItem = isSocial
        self.isNavigationButtonCellItem = isNav
        self.isJoinQueItem = isJoin
        self.isCreateQueItem = isCreate
        self.isSongItem = isSong
        self.isLikedItem = isLiked
        self.isPlaylistsItem = isPlaylist
    }
    
}



//Top navigation controller for both registering xib to collection and modifying it for presentation
class NavigationButtonCellController : CollectionCellController {

    fileprivate let item : CellItems

    init(item: CellItems) {
        self.item = item
    }
    
    fileprivate static var cellIdentifier: String {
        return String(describing: type(of: MusicNavigationButtonView.self))
    }
    
    static func registerCell(on collectionView: UICollectionView) {

        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
        
        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
        
        
        
    }

    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
        
        //Do modification here
        
        cell.contentView.isUserInteractionEnabled = true
        
        cell.backgroundColor = UIColor.systemRed
        print(item)
        if item.isJoinQueItem {
            print("isJoinQueItem")
            
            
            cell.actionButton.setTitle("Join Que", for: .normal)
        } else if item.isCreateQueItem {
            cell.actionButton.setTitle("Create Que", for: .normal)
            
        } else if item.isSongItem {
            cell.actionButton.setTitle("Songs", for: .normal)
        } else if item.isLikedItem {
            cell.actionButton.setTitle("Liked", for: .normal)
        } else if item.isPlaylistsItem {
            cell.actionButton.setTitle("Playlists", for: .normal)
        } else {
            print("Shouldn't get a MusicNavigationButtonCell like this @ cellFromCollectionView")
        }
        
        return cell
        
    }

    func didSelectCell() {
        
        if item.isJoinQueItem {
            
            
            NotificationCenter.default
            .post(name:           NSNotification.Name("joinQueButtonTapped"),
             object: nil)
        } else if item.isCreateQueItem {
            
            NotificationCenter.default
            .post(name:           NSNotification.Name("createQueButtonTapped"),
             object: nil)
            
        } else if item.isSongItem {
            NotificationCenter.default
            .post(name:           NSNotification.Name("songButtonTapped"),
             object: nil)
        } else if item.isLikedItem {
            NotificationCenter.default
            .post(name:           NSNotification.Name("likedButtonTapped"),
             object: nil)
        } else if item.isPlaylistsItem {
            NotificationCenter.default
            .post(name:           NSNotification.Name("playlistButtonTapped"),
             object: nil)
        } else {
            print("Shouldn't get a Musa `icNavigationButtonCell like this")
        }
    }

}

class MusicNavigationButtonView : UICollectionViewCell {
   
    @IBOutlet weak var actionButton: UIButton!
    
    
}



//class JoinQueButtonCellController : CollectionCellController {
//
//    fileprivate let item : HomeCellItems
//
//    init(item: HomeCellItems) {
//        self.item = item
//    }
//
//    fileprivate static var cellIdentifier: String {
//        return String(describing: type(of: MusicNavigationButtonView.self))
//    }
//
//    static func registerCell(on collectionView: UICollectionView) {
//
//        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
//
//        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
//
//
//
//    }
//
//    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
//
//        //Do modification here
//
//        cell.backgroundColor = UIColor.systemRed
//
//
//
//        return cell
//
//    }
//
//    func didSelectCell() {
//        print("Selected NavigationButtonCell")
//    }
//
//}
//
//class CreateQueButtonCellController : CollectionCellController {
//
//    fileprivate let item : HomeCellItems
//
//    init(item: HomeCellItems) {
//        self.item = item
//    }
//
//    fileprivate static var cellIdentifier: String {
//        return String(describing: type(of: MusicNavigationButtonView.self))
//    }
//
//    static func registerCell(on collectionView: UICollectionView) {
//
//        let xib = UINib.init(nibName:"MusicNavigationButtonView", bundle: nil)
//
//        collectionView.register(xib, forCellWithReuseIdentifier:"MusicNavigationButtonView")
//
//
//
//    }
//
//    func cellFromCollectionView(_ collectionView: UICollectionView, forIndexPath indexPath: IndexPath) -> UICollectionViewCell {
//
//        let cell : MusicNavigationButtonView = collectionView.dequeueReusableCell(withReuseIdentifier:"MusicNavigationButtonView", for: indexPath) as! MusicNavigationButtonView
//
//        //Do modification here
//
//        cell.backgroundColor = UIColor.systemRed
//
//
//
//        return cell
//
//    }
//
//    func didSelectCell() {
//        print("Selected NavigationButtonCell")
//    }
//
//}
//
