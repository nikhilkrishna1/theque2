//
//  Message.swift
//  theque
//
//  Created by Nikhil Krishna on 1/22/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import Foundation

class Message {
    
    var senderUID: String
    var receiverUIDs: [String]
    var content: String
    var timeSent: Date
    
    init (senderUID: String, receiverUIDs: [String], content: String, timeSent: Date) {
        
        self.senderUID = senderUID
        self.receiverUIDs = receiverUIDs
        self.content = content
        self.timeSent = timeSent
        
    }
    
    
}
