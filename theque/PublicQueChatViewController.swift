//
//  PublicQueChatViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/22/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import FirebaseFirestore

class PublicQueChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    // class variables
    var publicQue = PublicQue()
    var messages = [String]()
    
    // outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var messageField: UITextField!

        
    override func viewDidLoad() {
        super.viewDidLoad()

        loadMessages()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.navigationItem.title = "Chat"
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.messages.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "publicQueChatTableViewCell", for: indexPath) as! PublicQueChatTableViewCell

        // Configure the cell...
        cell.messageLabel.text = self.messages[indexPath.row]
        
        return cell
    }
    
    // MARK: Actions
    
    @IBAction func sendMessage() {
        
        self.publicQue.chatMessages.append(self.messageField.text!)
        self.tableView.reloadData()
        
        let db = Firestore.firestore()
        db.collection("Public Ques").document(self.publicQue.name).updateData([
            
            "chatMessages": FieldValue.arrayUnion([self.messageField.text!])
            
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                self.tableView.reloadData()
                print("Message Sent")
            }
        }
        
        self.tableView.reloadData()
        self.messageField.text = ""
    }
    
    
    // MARK: Text Field Functions
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (self.messageField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text.isEmpty {
            
            self.sendButton.isEnabled = false
            self.sendButton.alpha = 0.5
            
        } else {
            
            self.sendButton.isEnabled = true
            self.sendButton.alpha = 1.0
            
        }
        
        return true
    }
    
    
    //MARK: Helpers
    
    func loadMessages() {
        
        // contact Firestore and get database
        let db = Firestore.firestore()
        
        // get public que messages
        db.collection("Public Ques").getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    // get the que that matches name [MAKE UNIQUE ID LATER!]
                    if (document.get("name") as! String) == self.publicQue.name {
                        self.messages = document.get("chatMessages") as! [String]
                        self.tableView.reloadData()
                    }
                }
            }
        }
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
