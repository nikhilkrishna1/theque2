//
//  SpecificSongTableViewCell.swift
//  theque
//
//  Created by Nikhil Krishna on 2/7/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit

class SpecificSongTableViewCell: UITableViewCell {

    @IBOutlet weak var songName: UILabel!
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var artistName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
