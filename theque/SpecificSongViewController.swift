//
//  SpecificSongViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 2/6/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import Alamofire
import Firebase

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
        }.resume()
    }
    func downloaded(from link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}

class SpecificSongViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var publicQue: PublicQue!
    var song: [String:Any]!
    var related = [Song]()
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var foregroundImage: UIImageView!
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    typealias JSONstd = [String:AnyObject]


    override func viewDidLoad() {
        super.viewDidLoad()
        let imageData = song["spotifyID"]
        let imageURL = URL(string: imageData as! String)
        let mainImageData = NSData(contentsOf: imageURL!)
        let mainImage = UIImage(data: mainImageData! as Data)
        self.tableView.rowHeight = 75
        self.backgroundImage.image = mainImage
        self.foregroundImage.image = mainImage
        self.songLabel.text = song["title"] as? String
        self.artistName.text = song["artist"] as? String

        loadRelatedSongs()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.related.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "relatedCell") as! SpecificSongTableViewCell
        
        let songToDisplay = self.related[indexPath.row]
        
        cell.albumImage.image = songToDisplay.image!
        cell.songName.text = songToDisplay.title
        cell.artistName.text = songToDisplay.artist
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{

        return "Related to \(self.song["title"]!)"
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {


        let closeAction = UIContextualAction(style: .normal, title:  "Add to \(self.publicQue.name)", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            let db = Firestore.firestore()
            let publicQueRef = db.collection("Public Ques").document(self.publicQue.name)
            publicQueRef.updateData([
                
                "songs": FieldValue.arrayUnion([self.song_to_dict(song: self.related[indexPath.row])])
            ])
            success(true)
        })
        closeAction.backgroundColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        return UISwipeActionsConfiguration(actions: [closeAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        print(offset)
    }
    
    

    func loadRelatedSongs() {
        
        let track = song["spotifyURI"] as! String
        let index = track.index(track.startIndex, offsetBy: 14)
        let trackURI = track[index...]
        
        let accessToken = (UIApplication.shared.delegate as! AppDelegate).sessionManager.session?.accessToken
        let url = "https://api.spotify.com/v1/recommendations?seed_tracks=\(trackURI)"
        Alamofire.request(url, method: .get, headers: ["Authorization": "Bearer \(accessToken!)"]).responseJSON(completionHandler: {
            response in
            self.parsing(json: response.data!)
        })
        
            
    }
    func parsing(json: Data) {
        do {
            let readable = try JSONSerialization.jsonObject(with: json, options: .mutableContainers) as! JSONstd
            if let tracks = readable["tracks"] as? [JSONstd] {
                //print("TRACK:\n \n \n \n \n \(tracks[0]) \n \n \n \n \n \(tracks[0][""]!)")
                for track in tracks {
                    if let name = track["name"] as? String {
                        let uri = track["uri"] as! String
                        if let artists = track["artists"] as? [JSONstd] {
                            let album = track["album"] as? JSONstd
                            let images = album?["images"] as! [JSONstd]
                            let imageData = images[0]
                            let imageURL = URL(string: imageData["url"] as! String)
                            let mainImageData = NSData(contentsOf: imageURL!)
                            let mainImage = UIImage(data: mainImageData! as Data)
                            self.related.append(Song(title: name, artist: artists[0]["name"]as! String, image: mainImage!, spotifyID: imageData["url"] as! String, spotifyURI: uri))
                                self.tableView.reloadData()
                        }
                    }
                }
            }
            
       } catch {
           print("in here")
           print(error)
        }
    }
    
    
    func song_to_dict(song: Song) -> [String:Any] {
        
        let dict = ["title": song.title!, "artist": song.artist!, "spotifyID": song.spotifyID!, "spotifyURI": song.spotifyURI!] as [String : Any]
        return dict
        
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
