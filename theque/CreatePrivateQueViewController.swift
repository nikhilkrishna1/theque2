//
//  CreatePrivateQueViewController.swift
//  theque
//
//  Created by Nikhil Krishna on 1/14/20.
//  Copyright © 2020 The Que. All rights reserved.
//

import UIKit
import Contacts
import FirebaseFirestore
import FirebaseAuth

class CreatePrivateQueViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    // class variables and constants
    let letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    var contacts = [CNContact]()
    var dictOfLetters = [String:[CNContact]]()
    
    // outlets
    @IBOutlet weak var privateButton: UIBarButtonItem!
    @IBOutlet weak var finishedButton: UIButton!
    @IBOutlet weak var privateQueName: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        
        // Load contacts and table view
        getContacts()
        tableViewSetUp()
        
        self.privateQueName.delegate = self
        self.finishedButton.isEnabled = false
        self.finishedButton.alpha = 0.5
        
        self.navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        self.finishedButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        self.cancelButton.tintColor = #colorLiteral(red: 0.6727019548, green: 0.2218673825, blue: 1, alpha: 1)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for letter in letters {
            self.dictOfLetters[String(letter)] = []
        }
        
        for contact in self.contacts {
            if contact.givenName != "" {
                self.dictOfLetters[String(contact.givenName.first!)]?.append(contact)
            }
        }
    }
    
    // MARK: Table view data source
    
    func tableViewSetUp() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dictOfLetters.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "contactTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? CreatePrivateQueTableViewCell else {
            fatalError("The dequeued cell is not an instance of contactTableViewCell.")
        }
                
        let idx = letters.index(letters.startIndex, offsetBy: indexPath.section)
        let contact = self.dictOfLetters[String(letters[idx])]![indexPath.row]
        
        cell.contactNameLabel.text = contact.givenName + " " + contact.familyName
        cell.phoneNumbers = contact.phoneNumbers
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        let idx = letters.index(letters.startIndex, offsetBy: section)
        return self.dictOfLetters[String(letters[idx])]!.count
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {

        let arrayLetters = Array(letters).map{String($0)}
        return arrayLetters
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{

            return String(Array(letters)[section])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let idx = letters.index(letters.startIndex, offsetBy: indexPath.section)
        let selectedContact = self.dictOfLetters[String(letters[idx])]![indexPath.row]
        let phoneNumbersToAdd = selectedContact.phoneNumbers
        var actualNumbers = [String]()
        
        if phoneNumbersToAdd.count > 1 {
            for phoneNumber in phoneNumbersToAdd {
                actualNumbers.append(phoneNumber.value.stringValue)
            }
        } else if phoneNumbersToAdd.count != 0 {
            actualNumbers = [phoneNumbersToAdd[0].value.stringValue]
        }

        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
    
    // MARK: Actions
    
    @IBAction func toPublicCreator() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PublicQueCreator")
        self.show(vc, sender: self)
        
        
    }
    
    @IBAction func initializePrivate() {
        
        let db = Firestore.firestore()
        let userID = Auth.auth().currentUser!.uid
        
        db.collection("Private Ques").document(privateQueName.text!).setData([
            
            "name": self.privateQueName.text!,
            "songs": [],
            "members": [userID],
            "numUpvotes": [0],
            "likedSongsUsersPrivate": [[String:Bool]]()
            
        ]) {err in
            if err != nil {
                print("Somethin fucked up")
            } else {
                print("Database addition worked")
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
        
    @IBAction func returnToHome() {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    // MARK: Text Field Functions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = (self.privateQueName.text! as NSString).replacingCharacters(in: range, with: string)
        
        if text.isEmpty {
            
            self.finishedButton.isEnabled = false
            self.finishedButton.alpha = 0.5
            
        } else {
            
            self.finishedButton.isEnabled = true
            self.finishedButton.alpha = 1.0
            
        }
        
        return true
    }
    
    //MARK: Helpers
    
    func getContacts() {
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { (access, error) in
          print("Access: \(access)")
        }

        let keys = [CNContactFormatter.descriptorForRequiredKeys(for: .fullName), CNContactPhoneNumbersKey as CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: keys)
        
        do {
            try store.enumerateContacts(with: request) {
                (contact, stop) in
                // Array containing all unified contacts from everywhere
                self.contacts.append(contact)
            }
        }
        catch {
            print("unable to fetch contacts")
        }
    }
    
    
    func song_to_dict(song: Song) -> [String:Any] {
        
        let dict = ["title": song.title!, "artist": song.artist!] as [String : Any]
        return dict
        
    }
    

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
