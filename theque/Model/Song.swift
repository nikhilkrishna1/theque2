//
//  Song.swift
//  theque
//
//  Created by William Jacob Ballard on 10/4/19.
//  Copyright © 2019 The Que. All rights reserved.
//

import Foundation
import UIKit

public class Song  {
    
    var title : String?
    
    var artist : String?
    
    var image : UIImage?
    
    var isrc : String?
    
    var imageURL : String?

    var spotifyID: String?
    
    var spotifyURI: String?
    
    init(title: String, artist : String) {
        print("made it here")
        self.image = nil
        self.title = title
        self.artist = artist
    }
    
    init(title: String, artist : String, image: UIImage, spotifyID: String, spotifyURI: String) {
        self.image = image
        self.title = title
        self.artist = artist
        self.spotifyID = spotifyID
        self.spotifyURI = spotifyURI
    }
    

    init(title: String, artist : String, image: UIImage) {
        self.image = image
        self.title = title
        self.artist = artist
        self.spotifyID = ""
        self.spotifyURI = ""
    }
    
    init(title: String, artist: String, imageURL : String, isrc : String) {
        self.imageURL = imageURL
        self.image = nil
        self.isrc = isrc
        self.title = title
        self.artist = artist
        self.spotifyID = ""
        self.spotifyURI = ""
    }
    func equals(song: Song) -> Bool {
        return (self.spotifyURI == song.spotifyURI)
    }
    

//    func artworkURL(size dimension: Int = 100) -> URL? {
//        guard dimension > 0, dimension != 100,
//            var url = self.pictureURL else {
//                return self.pictureURL
//        }
//
//        url.deleteLastPathComponent()
//        url.appendPathComponent("\(dimension)x\(dimension)bb.jpg")
//
//        return url
//
//    }
    
    
}

//{"wrapperType":"track",
//"kind":"song",
//"artistId":909253,
//"collectionId":120954021,
//"trackId":120954025,
//"artistName":"Jack Johnson",
//"collectionName":"Sing-a-Longs and Lullabies for the Film Curious George",
//"trackName":"Upside Down",
//"collectionCensoredName":"Sing-a-Longs and Lullabies for the Film Curious George",
//"trackCensoredName":"Upside Down",
//"artistViewUrl":"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewArtist?id=909253",
//"collectionViewUrl":"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?i=120954025&id=120954021&s=143441",
//"trackViewUrl":"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewAlbum?i=120954025&id=120954021&s=143441",
//"previewUrl":"http://a1099.itunes.apple.com/r10/Music/f9/54/43/mzi.gqvqlvcq.aac.p.m4p",
//"artworkUrl60":"http://a1.itunes.apple.com/r10/Music/3b/6a/33/mzi.qzdqwsel.60x60-50.jpg",
//"artworkUrl100":"http://a1.itunes.apple.com/r10/Music/3b/6a/33/mzi.qzdqwsel.100x100-75.jpg",
//"collectionPrice":10.99,
//"trackPrice":0.99,
//"collectionExplicitness":"notExplicit",
//"trackExplicitness":"notExplicit",
//"discCount":1,
//"discNumber":1,
//"trackCount":14,
//"trackNumber":1,
//"trackTimeMillis":210743,
//"country":"USA",
//"currency":"USD",
//"primaryGenreName":"Rock"}
